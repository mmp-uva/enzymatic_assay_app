CHANGELOG
---------

0.1
---

-   First release. App uses two .xls files with data in first sheet and design in second sheet. Smoothing data curves with loess and calulate slopes by subtracting previous value. App can generate a .html report.
