Enzymatic acitvity assay app
----------------------------

A shiny app to analyze enzymatic activity assay data obtained from plate reader TBD.

Installation
------------

This app requieres the following packages

-   shiny
-   ggplot2
-   dplyr
-   tidyr
-   purrr
-   broom
-   readxl
-   lubridate
-   stringr
-   knitr

Usage
-----

The app expects two .xls/.xlsx files (background and assay) from the the plate reader data-export software (Mars? TDB). Both files should contain the exported data in the first sheet and the sample names in the second sheet. The latest should contain two columns, 'Content' and 'Definition'. The first column must contain the names reported by the plate reader ('Sample X1'). The second contains the desired sample name ('Strain023\#1'). For instance:

| Content   | Definition    |
|-----------|---------------|
| Sample X1 | Strain001\#1  |
| Sample X2 | Strain001\#02 |
| Sample X3 | Strain002\#01 |
